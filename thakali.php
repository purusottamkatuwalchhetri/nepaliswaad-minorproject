<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Every Product</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body> 
    <section id="header">
      <!-- <a href="index.php"><img src="img/fina_logo.png" class="logo" /></a> -->

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a class="active" href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          <!-- <li id="lg-bag">
            <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a>
          </li> -->
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <!-- <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a> -->
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="prodetails" class="section-p1">
      <div class="single-pro-image">
          <img src="img/products/Thakali-Khana-Set (1).jpg" width="100%" id="Main-Img" alt="">
           
          <div class="small-img-group" >
              <div class="small-img-col">
                  <img src="img/products/Jimbuthakali-1024x683.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/thakali_food.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/Thakali-Khana-Set (1).jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <!-- <img src="img/products/himalayan-dog-chew5.jpg" width="100%" class="small-img" alt=""> -->
              </div>
          </div>
      </div>

      <div class="single-pro-details">
          <h4>Thakali thali set</h4>
          <!-- <h4></h4> -->
          <h4>Product Details</h4>
          <span>	Community: Thakali Community <br> <br>

Food name: Thakali Khana Set<br><br>

Thakali Khana Set is a delicious main course dish that showcases the culinary expertise of the Thakali people, an ethnolinguistic group originating from the Thak Khola region of the Mustang District in Nepal. Renowned for their organizational skills and cleanliness, the Thakali people take pride in preparing meals that reflect their cultural heritage and traditions. <br><br>

Components of Thakali Khana Set: <br>
Dal (Lentil Soup): A hearty lentil soup seasoned with spices, providing a warm and comforting start to the meal. <br>
Bhat (Rice): Steamed white rice, a staple component of Nepalese cuisine, serves as the main source of carbohydrates. <br>
Tarkari (Vegetable Curry): A flavorful medley of seasonal vegetables cooked in aromatic spices, adding color and nutrients to the meal. <br>
Masu (Meat Curry): Tender pieces of meat, typically goat or chicken, cooked to perfection in a rich and savory gravy, offering protein and indulgent flavors. <br>
Achar (Pickles): Tangy and spicy pickles, such as tomato achar or radish achar, add a burst of flavor and contrast to the meal. <br>
Saag (Leafy Greens): Fresh, leafy greens, lightly sautéed with garlic and spices, provide a nutritious and refreshing accompaniment. <br><br>

Preparation and Serving: <br>
Thakali Khana Set is meticulously presented on a plate, with each component arranged neatly and thoughtfully. The harmonious combination of colors, textures, and flavors reflects the Thakali people's attention to detail and culinary expertise.
</span>
      </div>
  </section>

  <section id="product1" class="section-p1">
    <h2>Our Products</h2>
    <div class="pro-container">
      <div class="pro" onclick="window.location.href='dhikri.php';">
        <img src="img/products/dhikri1.jpg" alt="" />
        <div class="des">
          <span>Traditional Tharu dish</span>
          <h5>Dhikri</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a> -->
      </div>

      <div class="pro" onclick="window.location.href='thakali.php';">
        <img src="img/products/Thakali-Khana-Set (1).jpg" alt="" />
        <div class="des">
          <span>Thakali dish</span>
          <h5>Thakali khana set</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a>-->
      </div>

      <div class="pro" onclick="window.location.href='yomari.php';">
        <img src="img/products/yomari4.jpg" alt="" />
        <div class="des">
          <span>Newari dish</span>
          <h5>Yomari</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
        </div>
      </div>

      <div class="pro" onclick="window.location.href='selroti.php';">
        <img src="img/products/selroti1.jpg" alt="" />
        <div class="des">
          <span>Nepali Traditional dish</span>
          <h5>Selroti</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!-- <h4>Good</h4> -->
        </div>
        <!-- <a href="#"
          ><img
            src="img/products/cart-shopping-solid.svg"
            alt=""
            class="cart"
        /></a> -->
      </div>
    </div>
  </section>

  <footer class="section-p2">
    <div class="col">
      <!-- <img src="img/temporary_logo.png" alt=""> -->
      <h4>Contact</h4>
      <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
      <p><strong>Phone:</strong> 9818034478</p>
    
      <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
      <div class="follow">
        <h4>Follow Us</h4>
        <div class="icon">
            <i class="fab fa-facebook-f"></i>
            <i class="fab fa-twitter"></i>
            <i class="fab fa-instagram"></i>
        </div>
      </div>
    </div>

    <div class="col">
      <h4>About</h4>
      <a href="about.php">About us</a>
      <a href="privacy.php">Privacy Policy</a>
      <a href="#">Terms & Conditions</a>
      <a href="contact.php">Contact Us</a>
    </div>

    <div class="col">
      <h4>My Account</h4>
      <a href="logout.php">Sign out</a>
      <a href="indexCart.php">View Cart</a>
      <a href="#">My Wishlist</a>
      <a href="#">Help</a>
    </div>

    <div class="Payment">
      <h2><p>THANKS FOR CHOOSING US</p></h2>
    </div>

   
    </div>
</footer>

    <script>
      var MainImg = document.getElementById("Main-Img");
      var smallImg = document.getElementsByClassName("small-img");

      smallImg[0].onclick = function(){
        MainImg.src = smallImg[0].src; 
      }
      smallImg[1].onclick = function(){
        MainImg.src = smallImg[1].src; 
      }
      smallImg[2].onclick = function(){
        MainImg.src = smallImg[2].src; 
      }
      smallImg[3].onclick = function(){
        MainImg.src = smallImg[3].src; 
      }
    </script>

    <script src="script.js"></script>
  </body>
</html>
