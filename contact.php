<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Products</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>

    <section id="header">
      <!-- <a href="index.php"><img src="img/fina_logo.png" class="logo" /></a> -->

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a class="active" href="contact.php">Contact</a></li>
          <!-- <li id="lg-bag">
            <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a>
          </li> -->
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <!-- <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a> -->
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="page-header" class="about-header">
      <!-- <h1>Nepal</h1> -->
    </section>

    <section id="contact-details" class="section-p2">
      <div class="details">
        <!-- <h3> GET IN TOUCH </h3> -->
        <!-- <h2>Visit one of our agency locations or contact us today</h2> -->
        <div>
          <div>
            <p>Nepal</p>
          <li>
            <i class="fa-regular fa-map"></i>
            <p>Kalimati,Kathmandu,Nepal</p>
          </li>
          <li>
            <i class="fa-regular fa-envelope"></i>
            <p>nepaliswaad@gmail.com</p>
          </li>
          <li>
            <i class="fa-solid fa-phone"></i>
            <p>9818034478</p>
          </li>
          </div>
          <div>
            <!-- <p>Denmark</p> -->
          <li>
            <!-- <i class="fa-regular fa-map"></i> -->
            <!-- <p>frederikssundsvej 408<br>Zip code : 2700</p> -->
          </li>
          <li>
            <!-- <i class="fa-regular fa-envelope"></i> -->
            <!-- <p>nepaliswaad@gmail.com</p> -->
          </li>
          <li>
            <!-- <i class="fa-solid fa-phone"></i> -->
            <!-- <p>9818034478</p> -->
          </li>
          </div>
          <li >
            <i class="fa-regular fa-clock"></i>
            <p>Everyday</p>
          </li>
        </div>
      </div>

      <div class="map"> 
        
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28260.723797011633!2d85.27146433639227!3d27.699049525956397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1860ae22d385%3A0x7c2444e8284cef52!2sKalimati%2C%20Kathmandu%2044600!5e0!3m2!1sen!2snp!4v1705468257832!5m2!1sen!2snp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>"
          
      </div> 
    </section>

    <section id="form-details">
        <form action="contact_handler.php" method="POST">
            <span>LEAVE A MESSAGE </span>
            <h2>We love to hear from you</h2>
            <input type="text" placeholder="Your Name" name="name" required>
            <input type="text" placeholder="E-mail" name="email" required>
            <input type="text" placeholder="Subject" name="subject" required>
            <textarea name="message" id="" cols="30" rows="10" placeholder="Your Message" required></textarea>
            <input type="submit" name="submit" class="normal">
        </form>

        <div class="people">
            <div>
                <!-- <img src="img/people/1.png" alt=""> -->
                <p><span>Purusottam Katuwal Chhetri</span>Senior Marketing Manager <br>Phone: +000 123 000 77 88 <br> Email : contact@exapmel.com</p>
            </div>
            <div>
                <!-- <img src="img/people/2.png" alt=""> -->
                <p><span>Rubhon Dhungana</span>Senior Marketing Manager<br>Phone: +000 123 000 77 88 <br> Email : contact@exapmel.com</p>
            </div>
            <div>
                <!-- <img src="img/people/3.png" alt=""> -->
                <p><span>Sujit Rai</span>Senior Marketing Manager <br>Phone: +000 123 000 77 88 <br> Email : contact@exapmel.com</p>
            </div>
            <div>
              <!-- <img src="img/people/3.png" alt=""> -->
              <p><span>Santosh Thapa Chhetri</span>Senior Marketing Manager <br>Phone: +000 123 000 77 88 <br> Email : contact@exapmel.com</p>
          </div>

        </div>
    </section>

    <footer class="section-p2">
      <div class="col">
        <!-- <img src="img/temporary_logo.png" alt=""> -->
        <h4>Contact</h4>
        <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
        <p><strong>Phone:</strong>9818034478</p>
        <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
        <div class="follow">
          <h4>Follow Us</h4>
          <div class="icon">
              <i class="fab fa-facebook-f"></i>
              <i class="fab fa-twitter"></i>
              <i class="fab fa-instagram"></i>
          </div>
        </div>
      </div>

      <div class="col">
        <h4>About</h4>
        <a href="about.php">About us</a>
        <a href="privacy.php">Privacy Policy</a>
        <a href="#">Terms & Conditions</a>
        <a href="contact.php">Contact Us</a>
      </div>

      <div class="col">
        <h4>My Account</h4>
        <a href="logout.php">Sign out</a>
        <a href="indexCart.php">View Cart</a>
        <a href="#">My Wishlist</a>
        <a href="#">Help</a>
      </div>

      <div class="Payment">
        <h2><p>THANKS FOR CHOOSING US</p></h2>
      </div>

      
  </footer>

    <script src="script.js"></script>
  </body>
</html>
