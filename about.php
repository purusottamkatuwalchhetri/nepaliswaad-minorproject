<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Products</title>
    <script
      src="https://kit.fontawesome.com/906ae02b9f.js"
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
   

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>

          <li><a class="active" href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="page-header" class="about-header">
      
    </section>

    <section id="about-head" class="section">
      <img src="img/about/realabout.jpg" alt="" />
      <div>
        <h2>Welcome to Nepali Swaad – Where Sweetness Meets Tradition!</h2>
        <p>
          

Dive into a world of delight with "Nepali Swaad"! Discover the magic of traditional Nepali treats, seamlessly delivered to your doorstep. 
From easy orders to sweet transactions, join us in preserving the sweetness of Nepal's culinary heritage. Let every bite be a moment of pure joy. Your sweet adventure starts here!
        </p>
        
   

        
      </div>
    </section>

    <footer class="section-p2">
      <div class="col">
       
        <h4>Contact</h4>
        <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
        <p><strong>Phone:</strong> 9818034478</p>
        <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
        <div class="follow">
          <h4>Follow Us</h4>
          <div class="icon">
              <i class="fab fa-facebook-f"></i>
              <i class="fab fa-twitter"></i>
              <i class="fab fa-instagram"></i>
          </div>
        </div>
      </div>

      <div class="col">
        <h4>About</h4>
        <a href="about.php">About us</a>
        <a href="privacy.php">Privacy Policy</a>
        <a href="#">Terms & Conditions</a>
        <a href="contact.php">Contact Us</a>
      </div>

      <div class="col">
        <h4>My Account</h4>
        <a href="logout.php">Sign out</a>
        <a href="indexCart.php">View Cart</a>
        <a href="#">My Wishlist</a>
        <a href="#">Help</a>
      </div>

      <div class="Payment">
        <h2><p>THANKS FOR CHOOSING US</p></h2>
      </div>

  
  </footer>

    <script src="script.js"></script>
  </body>
</html>
