<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- <link rel="shortcut icon" type="x-icon" href="img/fina_logo.png"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <section id="header">
      <!-- <a href="index.php"><img src="img/fina_logo.png" class="logo" /></a> -->

      <div>
        <ul id="navbar">
          <li><a class="active" href="index.php">Home</a></li>
          <li><a href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          <!-- <li id="lg-bag">
            <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a>
          </li> -->
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <marquee behaviour = "scroll " scrollamount = "15" direction="right"> <h1 style="text-align: right; color: rgb(115, 171, 230);">Welcome to Nepali Swaad – Where Sweetness Meets Tradition!</h1> </marquee>
       <!-- <div id="mobile">
         <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a> 
        <i id="bar" class="fas fa-outdent"></i>
      </div>  -->
    </section>

    <section id="hero">
      <!--<h4>Trade-in-offer</h4>
      <h2>Super value deals</h2>
      <h1>On all products</h1>
      <p>Save more with coupons & up to 70% off!</p>
      <button>Shop Now</button>-->
    </section> 

    <section id="feature" class=" section-p1 ">
      <div class="fe-box">
        <img src="img/features/f1.png" alt="">
        <h6>Easy Access</h6>
      </div>

      <div class="fe-box">
        <img src="img/features/f2.png" alt="">
        <h6>Save TIme</h6>
      </div>

      <!-- <div class="fe-box">
        <img src="img/features/f3.png" alt="">
        <h6>Save Money</h6>
      </div> -->

      <!--<div class="fe-box">
        <img src="img/features/f4.png" alt="">
        <h6>Promotions</h6>
      </div>-->

      <div class="fe-box">
        <img src="img/features/f5.png" alt="">
        <h6>Quality Services</h6>
      </div>

      <div class="fe-box">
        <img src="img/features/f6.png" alt="">
        <h6>Customer Support</h6>
      </div>

    </section>
  
    <section id="product1" class="section-p1">
      <h2>Our Products</h2>
      <div class="pro-container">
        <div class="pro" onclick="window.location.href='dhikri.php';">
          <img src="img/products/dhikri1.jpg" alt="" />
          <div class="des">
            <span>Tharu traditional dish</span>
            <h5>Dhikri </h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <!--<h4>$78</h4>-->
          </div>
          <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a> -->
        </div>

        <div class="pro" onclick="window.location.href='thakali.php';">
          <img src="img/products/Thakali-Khana-Set (1).jpg" alt="" />
          <div class="des">
            <span>Thakali dish</span>
            <h5>Thakali khana set</h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <!--<h4>$78</h4>-->
          </div>
          <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a>-->
        </div>

        <div class="pro" onclick="window.location.href='yomari.php';">
          <img src="img/products/yomari4.jpg" alt="" />
          <div class="des">
            <span>Newari dish</span>
            <h5>yomari</h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
          </div>
        </div>

        <div class="pro" onclick="window.location.href='selroti.php';">
          <img src="img/products/selroti1.jpg" alt="" />
          <div class="des">
            <span>Traditional Nepali dish</span>
            <h5>Selroti</h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
            <!-- <h4>Good</h4> -->
          </div>
          <!-- <a href="#"
            ><img
              src="img/products/cart-shopping-solid.svg"
              alt=""
              class="cart"
          /></a> -->
        </div>
      </div>
      
    </section>
    <section id="product1" class="section-p1">
            
            
      <div class="pro-container">
        <div class="pro" onclick="window.location.href='gundruk.php';">
          <img src="img/products/gundruk2.jpg" alt="" />
          <div class="des">
            <span>Traditional Nepali dish </span>
            <h5>Gundruk</h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
          </div> 
        </div>
      

        <div class="pro" onclick="window.location.href='jujudhau.php';">
          <img src="img/products/juju2.jpg" alt="" />
          <div class="des">
            <span>Nepali dessert</span>
            <h5>Juju dhau</h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
          </div>
        </div>
        
      <div class="pro" onclick="window.location.href='bara.php';">
        <img src="img/products/bara1.jpg" alt="" />
        <div class="des">
          <span>Newari dish </span>
          <h5>Bara</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
         
        </div>
        
      </div>
      
        <div class="pro" onclick="window.location.href='chatamari.php';">
          <img src="img/products/chatamari.jpg" alt="" />
          <div class="des">
            <span>Newari dish </span>
            <h5>Chatamari</h5>
            <div class="star">
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
            </div>
           
          </div>
        

       
          </section>
          <section id="product1" class="section-p1">
            
            
            <div class="pro-container">
              <div class="pro" onclick="window.location.href='saphumicha.php';">
                <img src="img/products/1-2 (1).jpg" alt="" />
                <div class="des">
                  <span>Newari dish </span>
                  <h5>Saphu Mhicha</h5>
                  <div class="star">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </div>
                </div> 
              </div>
            
      
              <div class="pro" onclick="window.location.href='dhindho.php';">
                <img src="img/products/dhindo1.jpg" alt="" />
                <div class="des">
                  <span>Nepali Traditional Dish</span>
                  <h5>Dhindho</h5>
                  <div class="star">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </div>
                </div>
              </div>
              
            <div class="pro" onclick="window.location.href='ghongi.php';">
              <img src="img/products/ghongi2.jpg" alt="" />
              <div class="des">
                <span>Tharu dish </span>
                <h5>Ghongi</h5>
                <div class="star">
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                </div>
               
              </div>
              
            </div>
            
              <div class="pro" onclick="window.location.href='kheer.php';">
                <img src="img/products/kheer1.jpg" alt="" />
                <div class="des">
                  <span>Nepali Traditional dish </span>
                  <h5>Kheer</h5>
                  <div class="star">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                  </div>
                 
                </div>
              
      
             
                </section>
                <section id="product1" class="section-p1">
            
            
                  <div class="pro-container">
                    <div class="pro" onclick="window.location.href='kanchemba.php';">
                      <img src="img/products/kanchemba 1.png" alt="" />
                      <div class="des">
                        <span>Thakali dish </span>
                        <h5>Kanchemba</h5>
                        <div class="star">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                        </div>
                      </div> 
                    </div>
                  
            
                    <div class="pro" onclick="window.location.href='tilkoladdu.php';">
                      <img src="img/products/til-ladoo (1).webp" alt="" />
                      <div class="des">
                        <span>Nepali dessert</span>
                        <h5>Til Ko Laddu</h5>
                        <div class="star">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                        </div>
                      </div>
                    </div>
                    
                  <div class="pro" onclick="window.location.href='samayBaji.php';">
                    <img src="img/products/newari khaja set 1.jpg" alt="" />
                    <div class="des">
                      <span>Newari dish </span>
                      <h5>Samay Baji</h5>
                      <div class="star">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                      </div>
                     
                    </div>
                    
                  </div>
                  
                    <div class="pro" onclick="window.location.href='khariya.php';">
                      <img src="img/products/Khariya.jpg" alt="" />
                      <div class="des">
                        <span>Tharu dish </span>
                        <h5>Khariya</h5>
                        <div class="star">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                        </div>
                       
                      </div>
                    
            
                   
                      </section>
                      <section id="product1" class="section-p1">
            
            
                        <div class="pro-container">
                         
                  
                          <div class="pro" onclick="window.location.href='gengta.php';">
                            <img src="img/products/gengta.jpg" alt="" />
                            <div class="des">
                              <span>Tharu dish</span>
                              <h5>Gengta</h5>
                              <div class="star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                              </div>
                            </div>
                          </div>
                          
                        <div class="pro" onclick="window.location.href='bagiya.php';">
                          <img src="img/products/Bagiya Photo by Anita Chaudhary.jpg" alt="" />
                          <div class="des">
                            <span>Tharu dish </span>
                            <h5>Bagiya</h5>
                            <div class="star">
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                              <i class="fas fa-star"></i>
                            </div>
                           
                          </div>
                          
                        </div>
                        
                          <div class="pro" onclick="window.location.href='bhakka.php';">
                            <img src="img/products/Bhakka-1-1.jpg" alt="" />
                            <div class="des">
                              <span>Southeastern dish </span>
                              <h5>Bhakkha</h5>
                              <div class="star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                              </div>
                             
                            </div>
                          
                  
                         
                            </section>

    <footer class="section-p2">
        <div class="col">
          <!-- <img src="img/temporary_logo.png" alt=""> -->
          <h4>Contact</h4>
          <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
          <p><strong>Phone:</strong> 9818034478</p>
          <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
          <div class="follow">
            <h4>Follow Us</h4>
            <div class="icon">
                <i class="fab fa-facebook-f"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-instagram"></i>
            </div>
          </div>
        </div>

        <div class="col">
          <h4>About</h4>
          <a href="about.php">About us</a>
          <a href="privacy.php">Privacy Policy</a>
          <a href="#">Terms & Conditions</a>
          <a href="contact.php">Contact Us</a>
        </div>

        <div class="col">
          <h4>My Account</h4>
          <a href="logout.php">Sign Out</a>
          <a href="indexCart.php">View Cart</a>
          <a href="#">My Wishlist</a>
          <a href="#">Help</a>
        </div>

        <div class="Payment">
          <h2><p>THANKS FOR CHOOSING US</p></h2>
        

        
        
        
    
    <script src="script.js"></script>
  </body>
</html>
