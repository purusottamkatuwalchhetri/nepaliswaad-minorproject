<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Every Product</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body> 
    <section id="header">
      

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a class="active" href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="prodetails" class="section-p1">
      <div class="single-pro-image">
          <img src="img/products/newari khaja set 1.jpg" width="100%" id="Main-Img" alt="">
           
          <div class="small-img-group" >
              <div class="small-img-col">
                  <img src="img/products/newari khaja set 2.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/newari khaja set 1.jpg" width="100%" class="small-img" alt="">
              </div>
              


              
          </div>
      </div>

      <div class="single-pro-details">
          <span>Newari dish</span>
          <h4>Samay Baji</h4>
          <h4>Product Details</h4>
          <span>	Community: Newari Community<br><br>

Food name: Samay baji<br><br>

Samay Baji is a traditional dish of the Newar community in Nepal, celebrated for its authenticity and cultural significance. Over the years, it has gained popularity and become a focal point in Nepalese cuisine. Passed down through generations, Samay Baji holds a special place in the hearts of the people.
Samay Baji, a versatile and culturally significant dish of the Newar community in Nepal, is not bound by seasons. It graces auspicious occasions, family gatherings, and festivals year-round. As a starter, it plays a prominent role in celebrations, pujas, and even death anniversary rituals. Its simplicity and long shelf life make it a perennial favorite. A staple during major Nepali festivals like Indra Jatra, Dashain, and Tihar, Samay Baji is a common sight at Newari events, connecting generations through its flavors and traditions.
<br><br>
Components: <br>
1. Beaten rice: a type of flattened/beaten rice that is light and crispy. It is made by pounding rice in a mortar and pestle until it becomes thin and flat. <br>
2. Black soybeans (Bhatmas): These are lentils that have been roasted and seasoned. They are usually spicy and add a crunchy texture to the dish. <br>
3. Bara: It is a lentil patty as it is mainly made of lentils like "mugh beans" or "musur." <br>
4. Chatamari: It is a typical and traditional Newar dish that resembles pizza and has a variety of toppings<br>
5. Chhwela: It is one of the tastiest Newari dishes made by roasting the buffalo meat until the surface is charred, cooling it, then combining it with some spices and marinating it for a while. <br>
6. Aalu-wala: Aalu-wala is a spicy potato salad made by boiling potatoes until they are tender and mixed with different spices. <br>
7. Fried boiled eggs: Hard-boiled eggs are a common component of Samay Baji and add protein to the dish. The eggs are first boiled and then lightly fried until the outer parts are golden. <br>
8. Pickles: Pickles are a common condiment in Nepali cuisine and are often included in Samay Baji. <br><br>
Serving: <br>
Samay Baji is typically served on a single platter, accompanied by local Aila (spirit) or Chyaang (similar to white wine).
        </span>
      </div>
  </section>

  <section id="product1" class="section-p1">
    <h2>Our Products</h2>
    <div class="pro-container">
      <div class="pro" onclick="window.location.href='dhikri.php';">
        <img src="img/products/dhikri1.jpg" alt="" />
        <div class="des">
          <span>Traditional Tharu dish</span>
          <h5>Dhikri</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
       
      </div>

      <div class="pro" onclick="window.location.href='thakali.php';">
        <img src="img/products/Thakali-Khana-Set (1).jpg" alt="" />
        <div class="des">
          <span>Thakali dish</span>
          <h5>Thakali khana set</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a>-->
      </div>

      <div class="pro" onclick="window.location.href='yomari.php';">
        <img src="img/products/yomari4.jpg" alt="" />
        <div class="des">
          <span>Newari dish</span>
          <h5>Yomari</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
        </div>
      </div>

      <div class="pro" onclick="window.location.href='selroti.php';">
        <img src="img/products/selroti1.jpg" alt="" />
        <div class="des">
          <span>Traditional Nepali dish</span>
          <h5>selroti</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!-- <h4>Good</h4> -->
        </div>
        <!-- <a href="#"
          ><img
            src="img/products/cart-shopping-solid.svg"
            alt=""
            class="cart"
        /></a> -->
      </div>
    </div>
  </section>

  <footer class="section-p2">
    <div class="col">
      <!-- <img src="img/temporary_logo.png" alt=""> -->
      <h4>Contact</h4>
      <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
      <p><strong>Phone:</strong> 9818034478</p>
      <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
      <div class="follow">
        <h4>Follow Us</h4>
        <div class="icon">
            <i class="fab fa-facebook-f"></i>
            <i class="fab fa-twitter"></i>
            <i class="fab fa-instagram"></i>
        </div>
      </div>
    </div>

    <div class="col">
      <h4>About</h4>
      <a href="about.php">About us</a>
      <a href="privacy.php">Privacy Policy</a>
      <a href="#">Terms & Conditions</a>
      <a href="contact.php">Contact Us</a>
    </div>

    <div class="col">
      <h4>My Account</h4>
      <a href="logout.php">Sign out</a>
      <a href="indexCart.php">View Cart</a>
      <a href="#">My Wishlist</a>
      <a href="#">Help</a>
    </div>

    <div class="Payment">
      <h2><p>THANKS FOR CHOOSING US</p></h2>
    </div>

    
    </div>
</footer>

    <script>
      var MainImg = document.getElementById("Main-Img");
      var smallImg = document.getElementsByClassName("small-img");

      smallImg[0].onclick = function(){
        MainImg.src = smallImg[0].src; 
      }
      smallImg[1].onclick = function(){
        MainImg.src = smallImg[1].src; 
      }
      smallImg[2].onclick = function(){
        MainImg.src = smallImg[2].src; 
      }
      smallImg[3].onclick = function(){
        MainImg.src = smallImg[3].src; 
      }
    </script>

    <script src="script.js"></script>
  </body>
</html>
