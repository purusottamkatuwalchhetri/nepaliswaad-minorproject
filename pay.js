document.getElementById('paymentForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent form submission
  
    // Simulate payment processing
    setTimeout(function() {
      const paymentStatus = document.getElementById('paymentStatus');
      paymentStatus.textContent = 'Payment successful!';
      paymentStatus.style.color = 'green';
    }, 2000);
  });
  