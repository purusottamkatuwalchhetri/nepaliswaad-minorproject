<?php

$servername = "localhost";
$user = "root";
$password = "";
$dbname= "nepali_swad";
$conn = new mysqli($servername, $user, $password, $dbname);
$showAlert = false;
$showError = false;
$exists = false;

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $first_name = filter_input(INPUT_POST, "first_name", FILTER_SANITIZE_SPECIAL_CHARS);
    $last_name = filter_input(INPUT_POST, "last_name", FILTER_SANITIZE_SPECIAL_CHARS); 
    $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
    $phone_number = filter_input(INPUT_POST, "phone_number", FILTER_SANITIZE_SPECIAL_CHARS);
    $date= $_POST["date"];
    $gender= $_POST["Gender"];
    $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_SPECIAL_CHARS);
    $password = $_POST["password"];
    $hashed= password_hash("$password", PASSWORD_DEFAULT);

    $sql1="SELECT * from user_info where username='$username'";
    $result=mysqli_query($conn, $sql1);
    $num= mysqli_num_rows($result);
    if($num !=1 ){
      $exists = false;
    }
    else
    {
      $exists = true;
      $showError = "choose another username";
    }

       if($exists==false){
            $sql= "INSERT INTO nepali_swad.user_info(first_name, last_name, email, phone_number, date, gender, username, password)
            VALUES ('$first_name', '$last_name', '$email', '$phone_number', '$date', '$gender', '$username', '$hashed')";
            $result=mysqli_query($conn, $sql);
            if($result){
                $showAlert=true;
            }
       }
    }     

?>
<!DOCTYPE html>

<html lang="en" dir="ltr">
   <head>
      <meta charset="utf-8">
      <title>Registration Form</title>
      <link rel="stylesheet" href="styleSign.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
   </head>
   <body>
      <?php
         if($showAlert){
            echo'<div class="alert">
                     <strong>your account has been created</strong>
               </div>';
         }
         if($showError){
            echo'<div class="error">
            <strong>'.$showError.'</strong>
      </div>';
         }   
?>
      <div class="container">
         <header>Signup Form</header>
         <div class="progress-bar">
            <div class="step">
               <p>
                  Name
               </p>
               <div class="bullet">
                  <span>1</span>
               </div>
               <div class="check fas fa-check"></div>
            </div>
            <div class="step">
               <p>
                  Contact
               </p>
               <div class="bullet">
                  <span>2</span>
               </div>
               <div class="check fas fa-check"></div>
            </div>
            <div class="step">
               <p>
                  Birth
               </p>
               <div class="bullet">
                  <span>3</span>
               </div>
               <div class="check fas fa-check"></div>
            </div>
            <div class="step">
               <p>
                  Submit
               </p>
               <div class="bullet">
                  <span>4</span>
               </div>
               <div class="check fas fa-check"></div>
            </div>
         </div>
         <div class="form-outer">
            <form action="signUp.php" method="POST">
               <div class="page slide-page">
                  <div class="title">
                     Basic Info:
                  </div>
                  <div class="field">
                     <div class="label">
                        First Name
                     </div>
                     <input type="text" name="first_name" required>
                  </div>
                  <div class="field">
                     <div class="label">
                        Last Name
                     </div>
                     <input type="text" name="last_name" required>
                  </div>
                  
                  <div class="field">
                     <button class="firstNext next">Next</button>
                  </div>
                  <a href="login.php">Already Registered? go to login page</a>
               </div>
               
               <div class="page">
                  <div class="title">
                     Contact Info:
                  </div>
                  <div class="field">
                     <div class="label">
                        Email Address
                     </div>
                     <input type="text" name="email" required>
                  </div>
                  <div class="field">
                     <div class="label">
                        Phone Number
                     </div>
                     <input type="Number" name="phone_number" required>
                  </div>
                  <div class="field btns">
                     <button class="prev-1 prev">Previous</button>
                     <button class="next-1 next">Next</button>
                  </div>
               </div>
               <div class="page">
                  <div class="title">
                     Date of Birth:
                  </div>
                  <div class="field">
                     <div class="label">
                        Date
                     </div>
                     <input type="date" name="date" required>
                  </div>
                  <div class="field">
                     <div class="label">
                        Gender
                     </div>
                     <select name="Gender" required>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                     </select>
                  </div>
                  <div class="field btns">
                     <button class="prev-2 prev">Previous</button>
                     <button class="next-2 next">Next</button>
                  </div>
               </div>
               <div class="page">
                  <div class="title">
                     Login Details:
                  </div>
                  <div class="field">
                     <div class="label">
                        Username
                     </div>
                     <input type="text" name="username" required>
                  </div>
                  <div class="field">
                     <div class="label">
                        Password
                     </div>
                     <input type="password" name="password" required>
                  </div>
                  <div class="field btns">
    <button class="prev-3 prev">Previous</button>
    <button class="submit" type="submit" name="submit" onclick="redirectToLogin()">Submit</button>

    
</div>
               </div>
            </form>
         </div>
      </div>
      <script src="scriptSign.js"></script>

      <script>
    function redirectToLogin() {
        window.location.href = "login.php";
    }
</script>


   </body>
</html>