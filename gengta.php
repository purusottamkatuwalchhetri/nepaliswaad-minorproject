<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Every Product</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body> 
    <section id="header">
      

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a class="active" href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="prodetails" class="section-p1">
      <div class="single-pro-image">
          <img src="img/products/gengta.jpg" width="100%" id="Main-Img" alt="">
           
          <div class="small-img-group" >
              <div class="small-img-col">
                  <img src="img/products/gengta2.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/gengta.jpg" width="100%" class="small-img" alt="">
              </div>
              

              
          </div>
      </div>

      <div class="single-pro-details">
          <span>Tharu dish</span>
          <h4>Gengta</h4>
          <h4>Product Details</h4>
          <span>
          	Community: Tharu Community<br><br>

Food name: Gengta<br><br>

Gengta, a culinary gem in Tharu cuisine, is a special crab dish reserved for celebratory occasions among the Tharu community. This delectable preparation involves cooking the crabs with an array of spices, creating a flavorful and aromatic delicacy that holds a special place in Tharu culinary traditions. Known for its rich taste and unique preparation, Gengta stands out as one of the most popular delicacies enjoyed by the Tharu communities residing in the lowlands of Nepal. The dish not only serves as a testament to the cultural richness of Tharu culinary heritage but also adds a savory touch to festive gatherings and special events.

<br><br>
Ingredients: <br>
1. Fresh crabs<br>
2. Cooking oil<br>
3. Onion, garlic, ginger<br>
4. Tomatoes<br>
5. Green chilies <br>
6. Turmeric powder<br> 
7. Coriander powder<br>
8. Salt<br><br>
 
Preparation: <br>
First clean the crabs to remove dirt and debris. Break the claws and crack shells slightly. In a large pan, heat the oil and sauté onions, garlic, and ginger until golden. Add tomatoes, green chilies and cook until tomatoes form a thick gravy. Then add turmeric, red chili powder, coriander powder, salt. Adjust spice levels according to the need. Add the cleaned crabs and  mix it with the spice mixture. Cover the pan and cook for 15-20 minutes until fully cooked. Stir occasionally to ensure even cooking. At last garnish with fresh coriander leaves. 
<br><br>
Serving: <br>
This Gengta recipe captures the essence of the Tharu crab dish, offering a flavorful and aromatic experience. Adjust the spice levels and ingredients to suit your taste preferences. Enjoy this special delicacy on celebratory occasions or whenever you crave a taste of authentic Tharu cuisine


            </span>
      </div>
  </section>

  <section id="product1" class="section-p1">
    <h2>Our Products</h2>
    <div class="pro-container">
      <div class="pro" onclick="window.location.href='dhikri.php';">
        <img src="img/products/dhikri1.jpg" alt="" />
        <div class="des">
          <span>Traditional Tharu dish</span>
          <h5>Dhikri</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
       
      </div>

      <div class="pro" onclick="window.location.href='thakali.php';">
        <img src="img/products/Thakali-Khana-Set (1).jpg" alt="" />
        <div class="des">
          <span>Thakali dish</span>
          <h5>Thakali khana set</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a>-->
      </div>

      <div class="pro" onclick="window.location.href='yomari.php';">
        <img src="img/products/yomari4.jpg" alt="" />
        <div class="des">
          <span>Newari dish</span>
          <h5>Yomari</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
        </div>
      </div>

      <div class="pro" onclick="window.location.href='selroti.php';">
        <img src="img/products/selroti1.jpg" alt="" />
        <div class="des">
          <span>Traditional Nepali dish</span>
          <h5>selroti</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!-- <h4>Good</h4> -->
        </div>
        <!-- <a href="#"
          ><img
            src="img/products/cart-shopping-solid.svg"
            alt=""
            class="cart"
        /></a> -->
      </div>
    </div>
  </section>

  <footer class="section-p2">
    <div class="col">
      <!-- <img src="img/temporary_logo.png" alt=""> -->
      <h4>Contact</h4>
      <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
      <p><strong>Phone:</strong> 9818034478</p>
      <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
      <div class="follow">
        <h4>Follow Us</h4>
        <div class="icon">
            <i class="fab fa-facebook-f"></i>
            <i class="fab fa-twitter"></i>
            <i class="fab fa-instagram"></i>
        </div>
      </div>
    </div>

    <div class="col">
      <h4>About</h4>
      <a href="about.php">About us</a>
      <a href="privacy.php">Privacy Policy</a>
      <a href="#">Terms & Conditions</a>
      <a href="contact.php">Contact Us</a>
    </div>

    <div class="col">
      <h4>My Account</h4>
      <a href="logout.php">Sign out</a>
      <a href="indexCart.php">View Cart</a>
      <a href="#">My Wishlist</a>
      <a href="#">Help</a>
    </div>

    <div class="Payment">
      <h2><p>THANKS FOR CHOOSING US</p></h2>
    </div>

    
    </div>
</footer>

    <script>
      var MainImg = document.getElementById("Main-Img");
      var smallImg = document.getElementsByClassName("small-img");

      smallImg[0].onclick = function(){
        MainImg.src = smallImg[0].src; 
      }
      smallImg[1].onclick = function(){
        MainImg.src = smallImg[1].src; 
      }
      smallImg[2].onclick = function(){
        MainImg.src = smallImg[2].src; 
      }
      smallImg[3].onclick = function(){
        MainImg.src = smallImg[3].src; 
      }
    </script>

    <script src="script.js"></script>
  </body>
</html>
