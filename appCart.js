let openShopping = document.querySelector('.shopping');
let closeShopping = document.querySelector('.closeShopping');
let list = document.querySelector('.list');
let listCart = document.querySelector('.listCart');
let body = document.querySelector('body');
let total = document.querySelector('.total');
let quantity = document.querySelector('.quantity');

openShopping.addEventListener('click', ()=>{
    body.classList.add('active');
})
closeShopping.addEventListener('click', ()=>{
    body.classList.remove('active');
})

let products = [
    {
        id: 1,
        name: 'Dhikri',
        image: 'dhikri.jpg',
        price: 120
    },
    {
        id: 2,
        name: 'Thakali khana set',
        image:'Thakali-Khana-Set (1).jpg',
        price: 550
    },
    {
        id: 3,
        name: 'yomari',
        image: 'yomari4.jpg',
        price: 75
    },
    {
        id: 4,
        name: 'Selroti',
        image: 'selroti1.jpg',
        price: 50
    },
    {
        id: 5,
        name: 'Gundruk',
        image: 'gundruk2.jpg',
        price: 150
    },
    {
        id: 6,
        name: 'Juju Dhau',
        image: 'juju2.jpg',
        price: 75
    },
    {
        id: 7,
        name: 'Bara',
        image: 'bara1.jpg',
        price: 105
    },
    {
        id: 8,
        name: 'Chatamari',
        image: 'chatamari.jpg',
        price: 105
    },
    {
        id: 9,
        name: 'Saphu Mhicha',
        image: '1-2.jpg',
        price: 150
    },
    {
        id: 10,
        name: 'Dhindho',
        image: 'dhindo1.jpg',
        price: 550
    },
    {
        id: 11,
        name: 'Ghonghi',
        image: 'ghongi1.jpg',
        price: 250
    },
    {
        id: 12,
        name: 'kheer',
        image: 'kheer1.jpg',
        price: 200
    },
    {
        id: 13,
        name: 'Kanchemba',
        image: 'kanchemba 1.png',
        price: 300
    },
    {
        id: 14,
        name: 'Til Ko Laddu',
        image: 'til-ke-ladoo 2.jpg',
        price: 100
    },
    {
        id: 15,
        name: 'Samay Baji',
        image: 'newari khaja set 1.jpg',
        price: 305
    },
    {
        id: 16,
        name: 'Khariya',
        image: 'Khariya.jpg',
        price: 85
    },
    {
        id: 17,
        name: 'Gengta',
        image: 'gengta.jpg',
        price: 205
    },
    {
        id: 18,
        name: 'Bagiya',
        image: 'Bagiya.jpg',
        price: 75
    },
    {
        id: 19,
        name: 'Bhakka',
        image: 'bhakka.jpg',
        price: 90
    }
    

];
let listCarts  = [];
function initApp(){
    products.forEach((value, key) =>{
        let newDiv = document.createElement('div');
        newDiv.classList.add('item');
        newDiv.innerHTML = `
            <img src="image/${value.image}">
            <div class="title">${value.name}</div>
            <div class="price">${value.price.toLocaleString()}</div>
            <button onclick="addToCart(${key})">Add To Cart</button>`;
        list.appendChild(newDiv);
    })
}
initApp();
function addToCart(key){
    if(listCarts[key] == null){
        // copy product form list to list cart
        listCarts[key] = JSON.parse(JSON.stringify(products[key]));
        listCarts[key].quantity = 1;
    }
    reloadCart();
}
function reloadCart(){
    listCart.innerHTML = '';
    let count = 0;
    let totalPrice = 0;
    listCarts.forEach((value, key)=>{
        totalPrice = totalPrice + value.price;
        count = count + value.quantity;
        if(value != null){
            let newDiv = document.createElement('li');
            newDiv.innerHTML = `
                <div><img src="image/${value.image}"/></div>
                <div>${value.name}</div>
                <div>${value.price.toLocaleString()}</div>
                <div>
                    <button onclick="changeQuantity(${key}, ${value.quantity - 1})">-</button>
                    <div class="count">${value.quantity}</div>
                    <button onclick="changeQuantity(${key}, ${value.quantity + 1})">+</button>
                </div>`;
                listCart.appendChild(newDiv);
        }
    })
    total.innerText = totalPrice.toLocaleString();
    quantity.innerText = count;
}
function changeQuantity(key, quantity){
    if(quantity == 0){
        delete listCarts[key];
    }else{
        listCarts[key].quantity = quantity;
        listCarts[key].price = quantity * products[key].price;
    }
    reloadCart();
}