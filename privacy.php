<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Privacy Policy</title>
    <link rel="stylesheet" href="style.css" />


    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f7f7f7;
            color: #333;
            background-image: url("img/background/pokhara.jpg");
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            background-color: #fff;
            padding: 30px;
            border-radius: 8px;
            box-shadow: 0 0 20px rgba(0,0,0,0.1);
            background-image: url("img/background/pokhara.jpg");
        }
        h1, h2 {
            text-align: center;
            color: #333;
            margin-bottom: 20px;
        }
        p {
            margin-bottom: 15px;
            color: #666;
            line-height: 1.6;
        }
        a {
            color: #007bff;
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <section id="about-head" class="section">
        <div>
          <h2>Welcome to Nepali Swaad – Where Sweetness Meets Tradition!</h2>
          <p>
            
  
  Dive into a world of delight with "Nepali Swaad"! Discover the magic of traditional Nepali treats, seamlessly delivered to your doorstep. 
  From easy orders to sweet transactions, join us in preserving the sweetness of Nepal's culinary heritage. Let every bite be a moment of pure joy. Your sweet adventure starts here!
          </p>
          
     
  
          
        </div>
      </section>
    <div class="container">
        <h1>Privacy Policy</h1>
        <p>Welcome to our Privacy Policy page. Here, we outline the types of personal information we receive and collect when you use our website and the steps we take to safeguard your information. We hope this will help you make an informed decision about sharing personal information with us.</p>

        <h2>Information Collection and Use</h2>
        <p>We respect your privacy. Any personal information you provide to us, including your name, address, email address, or phone number, will not be released, sold, or rented to any entities or individuals outside of our organization except as noted below.</p>

        <h2>Log Data</h2>
        <p>Like many website operators, we collect information that your browser sends whenever you visit our website. This log data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, pages of our website that you visit, the time and date of your visit, the time spent on those pages, and other statistics.</p>

        <h2>Cookies</h2>
        <p>Cookies are files with a small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your computer's hard drive. We use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our website.</p>

        <h2>Security</h2>
        <p>The security of your personal information is important to us, but remember that no method of transmission over the Internet or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security.</p>

        <h2>Changes to This Privacy Policy</h2>
        <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page. You are advised to review this Privacy Policy periodically for any changes.</p>

        <h2>Contact Us</h2>
        <p>If you have any questions about our Privacy Policy, please <a href="contact.php">contact us</a> through our website.</p>
    </div>
    <footer class="section-p2">
        <div class="col">
         
          <h4>Contact</h4>
          <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
          <p><strong>Phone:</strong> 9818034478</p>
          <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
          <div class="follow">
            <h4>Follow Us</h4>
            <div class="icon">
                <i class="fab fa-facebook-f"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-instagram"></i>
            </div>
          </div>
        </div>
  
        <div class="col">
          <h4>About</h4>
          <a href="about.php">About us</a>
          <a href="privacy.php">Privacy Policy</a>
          <a href="#">Terms & Conditions</a>
          <a href="contact.php">Contact Us</a>
        </div>
  
        <div class="col">
          <h4>My Account</h4>
          <a href="logout.php">Sign out</a>
          <a href="indexCart.php">View Cart</a>
          <a href="#">My Wishlist</a>
          <a href="#">Help</a>
        </div>
  
        <div class="Payment">
          <h2><p>THANKS FOR CHOOSING US</p></h2>
        </div>
  
    
    </footer>
  
    
</body>
</html>
