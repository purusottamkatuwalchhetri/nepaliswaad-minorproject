<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styleCart.css">
    <link rel="stylesheet" href="styleForCart.css" />
    <script>
        src="https://kit.fontawesome.com/906ae02b9f.js"
      crossorigin="anonymous"
    </script>
</head>
<body class="">
    <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          <!-- <li id="lg-bag">
            <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a>
          </li> -->
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <!-- <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a> -->
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="page-header" class="about-header">
      <!-- <h1>Nepal</h1> -->
    </section>

    <section id="about-head" class="section">
      <!-- <img src="img/about/realabout.jpg" alt="" /> -->
      <div>
        <h2>Welcome to Nepali Swaad – Where Sweetness Meets Tradition!</h2>
        <p>
          

Dive into a world of delight with "Nepali Swaad"! Discover the magic of traditional Nepali treats, seamlessly delivered to your doorstep. 
From easy orders to sweet transactions, join us in preserving the sweetness of Nepal's culinary heritage. Let every bite be a moment of pure joy. Your sweet adventure starts here!
        </p>
        
   

        <!-- <marquee bgcolor="#ccc" loop = "-1" scrollamount = "5" width="100%">Proud to be Grokhali</marquee> -->
      </div>
    </section>
    
    <div class="container">
        <header>
            <h1>Your Shopping Cart</h1>
            <div class="shopping">
                <img src="image/shopping.svg">
                <span class="quantity">0</span>
            </div>
        </header>

        <div class="list">
          
        </div>
    </div>
    <div class="cart">
        <h1>Cart</h1>
        <ul class="listCart">
        </ul>
        <div class="checkOut">
          <a href="pay.php">
            <div class="total">0</div>
        </a>
        
              
             
            
            
            
            <div class="closeShopping">Close</div>
        </div>
    </div>
    <footer class="section-p2">
        <div class="col">
          
          <h4>Contact</h4>
          <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
          <p><strong>Phone:</strong> 9818034478</p>
          <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
          <div class="follow">
            <h4>Follow Us</h4>
            <div class="icon">
                <i class="fab fa-facebook-f"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-instagram"></i>
            </div>
          </div>
        </div>
  
        <div class="col">
          <h4>About</h4>
          <a href="about.php">About us</a>
          <a href="privacy.php">Privacy Policy</a>
          <a href="#">Terms & Conditions</a>
          <a href="contact.php">Contact Us</a>
        </div>
  
        <div class="col">
          <h4>My Account</h4>
          <a href="logout.php">Sign out</a>
          <a href="indexCart.php">View Cart</a>
          <a href="#">My Wishlist</a>
          <a href="#">Help</a>
        </div>
  
        <div class="Payment">
          <h2><p>THANKS FOR CHOOSING US</p></h2>
        </div>
  
    
    </footer>
    <script src="script.js"></script>

    <script src="appCart.js"></script>
</body>
</html>