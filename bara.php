<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Every Product</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body> 
    <section id="header">
      

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a class="active" href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="prodetails" class="section-p1">
      <div class="single-pro-image">
          <img src="img/products/bara1.jpg" width="100%" id="Main-Img" alt="">
           
          <div class="small-img-group" >
              <div class="small-img-col">
                  <img src="img/products/bara2.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/bara3.jpg" width="100%" class="small-img" alt="">
              </div>
              <div class="small-img-col">
                <img src="img/products/bara1.jpg" width="100%" class="small-img" alt="">
            </div>


              
          </div>
      </div>

      <div class="single-pro-details">
          <span>Newari dish</span>
          <h4>Bara</h4>
          <h4>Product Details</h4>
          <span>	Community: Newar Community<br><br>

Food name: Bara<br><br>

Bara, also known as Wo, is a popular Newari dish made from lentils. It is a type of savory pancake that is often served as a snack or appetizer. Bara can be made with different types of lentils, but the most common type is black lentils.Bara is often served at festivals, birthdays, and celebrations as a good-luck food.
<br><br>
Ingredients: <br>
  
1. Black lentils, soaked overnight<br>
2. Salt<br>
3. Cumin powder<br>
4. Coriander powder<br>
5. Asafoetida<br>
6. Red chili powder<br>
7. Garam masala<br>
8. Chopped cilantro<br>
9. Chopped onion<br>
10. Chopped green chilies<br>
11. Vegetable Oil<br><br>

Preparation and Serving: <br>
To prepare baras, begin by soaking black lentils overnight to soften them and kickstart the fermentation process, enhancing their digestibility and imparting a subtle tang. After draining and rinsing the lentils, grind them into a fine paste using a blender or food processor until smooth. Transfer the lentil paste to a mixing bowl and add salt, turmeric powder, cumin powder, coriander powder, asafoetida, red chili powder, and garam masala, ensuring an even distribution of spices. Finely chop cilantro, onion, and green chilies, gently folding them into the lentil batter for vibrant flavors. Heat a flat iron griddle called Tawa or non-stick pan over medium heat, adding a thin layer of oil. Pour a small amount of batter onto the pan, spreading it into a thin pancake. Fry the bara for 2-3 minutes per side until golden brown and crispy, repeating with the remaining batter. Baras are best enjoyed hot, straight off the pan, boasting a crispy exterior and a soft, fluffy interior. They make for a wholesome breakfast, snack, or light meal, perfect when paired with a steaming cup of tea or spiced milk tea. Additionally, they can be served with a variety of side dishes, sauces, pickles, or achar for added flavor and texture.</span>
      </div>
  </section>

  <section id="product1" class="section-p1">
    <h2>Our Products</h2>
    <div class="pro-container">
      <div class="pro" onclick="window.location.href='dhikri.php';">
        <img src="img/products/dhikri1.jpg" alt="" />
        <div class="des">
          <span>Traditional Tharu dish</span>
          <h5>Dhikri</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
       
      </div>

      <div class="pro" onclick="window.location.href='thakali.php';">
        <img src="img/products/Thakali-Khana-Set (1).jpg" alt="" />
        <div class="des">
          <span>Thakali dish</span>
          <h5>Thakali khana set</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a>-->
      </div>

      <div class="pro" onclick="window.location.href='yomari.php';">
        <img src="img/products/yomari4.jpg" alt="" />
        <div class="des">
          <span>Newari dish</span>
          <h5>Yomari</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
        </div>
      </div>

      <div class="pro" onclick="window.location.href='selroti.php';">
        <img src="img/products/selroti1.jpg" alt="" />
        <div class="des">
          <span>Traditional Nepali dish</span>
          <h5>selroti</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!-- <h4>Good</h4> -->
        </div>
        <!-- <a href="#"
          ><img
            src="img/products/cart-shopping-solid.svg"
            alt=""
            class="cart"
        /></a> -->
      </div>
    </div>
  </section>

  <footer class="section-p2">
    <div class="col">
      <!-- <img src="img/temporary_logo.png" alt=""> -->
      <h4>Contact</h4>
      <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
      <p><strong>Phone:</strong> 9818034478</p>
      <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
      <div class="follow">
        <h4>Follow Us</h4>
        <div class="icon">
            <i class="fab fa-facebook-f"></i>
            <i class="fab fa-twitter"></i>
            <i class="fab fa-instagram"></i>
        </div>
      </div>
    </div>

    <div class="col">
      <h4>About</h4>
      <a href="about.php">About us</a>
      <a href="privacy.php">Privacy Policy</a>
      <a href="#">Terms & Conditions</a>
      <a href="contact.php">Contact Us</a>
    </div>

    <div class="col">
      <h4>My Account</h4>
      <a href="logout.php">Sign out</a>
      <a href="indexCart.php">View Cart</a>
      <a href="#">My Wishlist</a>
      <a href="#">Help</a>
    </div>

    <div class="Payment">
      <h2><p>THANKS FOR CHOOSING US</p></h2>
    </div>

    
    </div>
</footer>

    <script>
      var MainImg = document.getElementById("Main-Img");
      var smallImg = document.getElementsByClassName("small-img");

      smallImg[0].onclick = function(){
        MainImg.src = smallImg[0].src; 
      }
      smallImg[1].onclick = function(){
        MainImg.src = smallImg[1].src; 
      }
      smallImg[2].onclick = function(){
        MainImg.src = smallImg[2].src; 
      }
      smallImg[3].onclick = function(){
        MainImg.src = smallImg[3].src; 
      }
    </script>

    <script src="script.js"></script>
  </body>
</html>
