<?php

$servername = "localhost";
$user = "root";
$password = "";
$dbname = "nepali_swad";

$conn = mysqli_connect($servername, $user, $password, $dbname);
$login = false;
$showError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = filter_input(INPUT_POST, "username", FILTER_SANITIZE_SPECIAL_CHARS);
    $password = $_POST["password"];

    $sql = "SELECT * from user_info where username='$username'";
    $result = mysqli_query($conn, $sql);

    if ($result) {
      $row = mysqli_fetch_assoc($result);
  
      if ($row) {
          if (password_verify($password, $row['password'])) {
              $login = true;
              session_start();
              $_SESSION["loggedin"] = true;
              header("location: index.php");
          } else {
              $showError = "Invalid password";
          }
      } else {
          $showError = "No user found with the provided username";
      }
  } else {
      $showError = "Error executing the query: " . mysqli_error($conn);
  }
  
}

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> Login Form </title>
    <link rel="stylesheet" href="styleLogin.css">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  <?php
         if($login){
            echo'<div class="alert">You are logged in</div>';
         }
         if($showError){
            echo'<div class="error">'.$showError.'</div>';
         }   
?>
    <div>
      <ul id="navbar">
        <li><a href="index.html">Home</a></li>
        <li><a href="shop.html">Products</a></li>
        <li><a href="indexCart.html">Order Items</a></li>
        <li><a href="about.html">About</a></li>
        <li><a href="contact.html">Contact</a></li>
        
        <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
      </ul>
    </div>
    <div id="mobile">
      
      <i id="bar" class="fas fa-outdent"></i>
    </div>
    <div class="center">
      <h1>Login</h1>
        <form method="post" action="login.php">

        <div class="txt_field">
          <input type="text" name="username" required>
          <span></span>
          <label>Username</label>
        </div>
        <div class="txt_field">
          <input type="password" name="password" required>
          <span></span>
          <label>Password</label>
        </div>
        <div class="pass">Forgot Password?</div>
        <input type="submit" value="Login">
        <div class="signup_link">
          Not a member? <a href="signUp.php">Signup</a>
        </div>
      </form>
    </div>
    

  </body>
  <script src="login.js"></script>
</html>