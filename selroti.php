<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Every Product</title>
    <script src="https://kit.fontawesome.com/906ae02b9f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body> 
    <section id="header">
      

      <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a class="active" href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          <!-- <li id="lg-bag">
            <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a>
          </li> -->
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
        <!-- <a href="cart.php"><i class="fa-solid fa-bag-shopping"></i></a> -->
        <i id="bar" class="fas fa-outdent"></i>
      </div>
    </section>

    <section id="prodetails" class="section-p1">
      <div class="single-pro-image">
          <img src="img/products/selroti1.jpg" width="100%" id="Main-Img" alt="">
           
          <div class="small-img-group" >
              <div class="small-img-col">
                  <img src="img/products/selroti.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/selroti1.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/selroti3.jpg" width="100%" class="small-img" alt="">
              </div>

              <div class="small-img-col">
                  <img src="img/products/selroti4.jpg" width="100%" class="small-img" alt="">
              </div>
          </div>
      </div>

      <div class="single-pro-details">
          <h4>Selroti</h4>
          <h4>Traditional Nepali dish</h4>
          <h4>Product Details</h4>
          <span> 	Community: Nepalese Community<br><br>

Food name: Selroti<br><br>

Sel roti, known as "सेल रोटी" in Nepali, is a beloved traditional delicacy in Nepal, particularly during the festive seasons of Dashain and Tihar. This ring-shaped sweet fried dough, made from rice flour, holds cultural significance as it is often shared as gifts among family members and used as prasada, a religious offering. <br><br>

Ingredients: <br>
1. Rice flour<br>
2. Water<br>
3. Sugar<br>
4. Ghee<br>
5. Cardamom (optional) <br>
6. Clove (optional) <br>
<br>
Preparation: <br>
First a batter is prepared by adding water, sugar, and ghee to roughly grinded rice flour. The ghee can also be substituted by cooking oil or butter. Spices such as cardamom and clove could also be added to the batter. The ingredients are then let to rest for a couple of hours. Once the batter is set, it is deep fried in cooking oil or ghee. The dough is poured by hand in a ring shape into boiling oil and cooked on medium heat until it turns light brown on both sides. Two wooden sticks (known as jhir in Nepali language), are used for turning the dough while cooking.Sel roti are cooked in bulk and can be stored at room temperature for least 20 days. <br><br>

Serving: <br>
Sel roti is best enjoyed warm and freshly fried. When it’s straight out of the oil, it’s at its most delicious. It also pairs wonderfully with tea and other hot beverages. The combination of the crispy, slightly sweet sel roti with a warm cup of tea is a delightful experience. You can also serve sel roti with various accompaniments such as yogurt, hot beverages and pickles.
</span>
      </div>
  </section>

  <section id="product1" class="section-p1">
    <h2>Our Products</h2>
    <div class="pro-container">
      <div class="pro" onclick="window.location.href='dhikri.php';">
        <img src="img/products/dhikri1.jpg" alt="" />
        <div class="des">
          <span>Traditional Tharu dish</span>
          <h5>Dhikri</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a> -->
      </div>

      <div class="pro" onclick="window.location.href='thakali.php';">
        <img src="img/products/Thakali-Khana-Set (1).jpg" alt="" />
        <div class="des">
          <span>Thakali dish</span>
          <h5>Thakali khana set</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!--<h4>$78</h4>-->
        </div>
        <!--<a href="#"><img src="img/products/cart-shopping-solid.svg" alt="" class="cart"></a>-->
      </div>

      <div class="pro" onclick="window.location.href='yomari.php';">
        <img src="img/products/yomari4.jpg" alt="" />
        <div class="des">
          <span>Newari dish</span>
          <h5>Yomari</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
        </div>
      </div>

      <div class="pro" onclick="window.location.href='selroti.php';">
        <img src="img/products/selroti1.jpg" alt="" />
        <div class="des">
          <span>Traditional Nepali dish</span>
          <h5>Selroti</h5>
          <div class="star">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
          </div>
          <!-- <h4>Good</h4> -->
        </div>
        <!-- <a href="#"
          ><img
            src="img/products/cart-shopping-solid.svg"
            alt=""
            class="cart"
        /></a> -->
      </div>
    </div>
  </section>

  <footer class="section-p2">
    <div class="col">
      <!-- <img src="img/temporary_logo.png" alt=""> -->
      <h4>Contact</h4>
      <p><strong>Address:</strong>Kalimati,Kathmandu, Nepal </p>
      <p><strong>Phone:</strong> 9818034478</p>
    
      <p><strong>E-mail id:</strong>nepaliswaad@gmail.com</p>
      <div class="follow">
        <h4>Follow Us</h4>
        <div class="icon">
            <i class="fab fa-facebook-f"></i>
            <i class="fab fa-twitter"></i>
            <i class="fab fa-instagram"></i>
        </div>
      </div>
    </div>

    <div class="col">
      <h4>About</h4>
      <a href="about.php">About us</a>
      <a href="privacy.php">Privacy Policy</a>
      <a href="#">Terms & Conditions</a>
      <a href="contact.php">Contact Us</a>
    </div>

    <div class="col">
      <h4>My Account</h4>
      <a href="logout.php">Sign out</a>
      <a href="indexCart.php">View Cart</a>
      <a href="#">My Wishlist</a>
      <a href="#">Help</a>
    </div>

    <div class="Payment">
      <h2><p>THANKS FOR CHOOSING US</p></h2>
    </div>

   
</footer>

    <script>
      var MainImg = document.getElementById("Main-Img");
      var smallImg = document.getElementsByClassName("small-img");

      smallImg[0].onclick = function(){
        MainImg.src = smallImg[0].src; 
      }
      smallImg[1].onclick = function(){
        MainImg.src = smallImg[1].src; 
      }
      smallImg[2].onclick = function(){
        MainImg.src = smallImg[2].src; 
      }
      smallImg[3].onclick = function(){
        MainImg.src = smallImg[3].src; 
      }
    </script>

    <script src="script.js"></script>
  </body>
</html>
