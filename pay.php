<?php
      session_start();

      if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"]!=true){
        header("location: login.php");
        exit;
      }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Payment Form</title>
  <link rel="stylesheet" href="StylePay.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>
    <div>
        <ul id="navbar">
          <li><a href="index.php">Home</a></li>
          <li><a href="shop.php">Products</a></li>
          <li><a href="indexCart.php">Order Items</a></li>
          <li><a href="about.php">About</a></li>
          <li><a href="contact.php">Contact</a></li>
          
          <a href="#" id="close"><i class="fa -solid fa-xmark"></i></a>
        </ul>
      </div>
      <div id="mobile">
  <div class="container">
    <h1>Payment Form</h1>
    <form id="paymentForm">
      <div class="form-group">
        <label for="cardNumber">Card Number</label>
        <input type="text" id="cardNumber" name="cardNumber" placeholder="1234 5678 9101 1121" required>
      </div>
      <div class="form-group">
        <label for="expiryDate">Expiry Date</label>
        <input type="text" id="expiryDate" name="expiryDate" placeholder="MM/YY" required>
      </div>
      <div class="form-group">
        <label for="cvv">CVV</label>
        <input type="text" id="cvv" name="cvv" placeholder="123" required>
      </div>
      <div class="form-group">
        <label for="amount">Amount</label>
        <input type="text" id="amount" name="amount" placeholder="100.00" required>
      </div>
      <button type="submit" id="payButton">Pay Now</button>
    </form>
    <div id="paymentStatus"></div>
  </div>

  <script src="pay.js"></script>
</body>
</html>
